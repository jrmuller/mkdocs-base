
stathat module
**************


stathat.py
==========

A minimalistic API wrapper for StatHat.com, powered by Requests.

Usage:

   >>> from stathat import StatHat
   >>> stats = StatHat('me@kennethreitz.com')
   >>> stats.count('wtfs/minute', 10)
   True
   >>> stats.count('connections.active', 85092)
   True

Enjoy.

class stathat.StatHat(email=None)

   Bases: "object"

   The StatHat API wrapper.

   STATHAT_URL = 'http://api.stathat.com'

   count(key, count)

   value(key, value)
