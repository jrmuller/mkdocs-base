.. meta data placeholder

.. include:: metadata_header.rst

modules
=======

Een voorbeeld van een link

External hyperlinks, like Python_.

.. _Python: http://www.python.org/


.. code-block:: python
   :emphasize-lines: 3,5

   def some_function():
       interesting = False
       print 'This line is highlighted.'
       print 'This one is not...'
       print '...but this one is.'


|release|

|version|

|today|

.. met ifconfig moet het mogelijk zijn rekening te houden met output html of output pdf (scale 85 %)