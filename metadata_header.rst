.. metadata-placeholder

:DC.Title:
   Document title
:DC.Creator:
   Author
:DC.Date:
   Date yyyy-mm-dd
:DC.Description:
   Abstract
:DC.Language:
   en
:DC.Format:
   text/x-rst
:DC.Rights:
   Access rights
:DC.RightsHolder:
   Copyright.